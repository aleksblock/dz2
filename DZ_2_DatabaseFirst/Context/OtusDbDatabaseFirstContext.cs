﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using DZ_2_DatabaseFirst.Models;

namespace DZ_2_DatabaseFirst.Context
{
    public partial class OtusDbDatabaseFirstContext : DbContext
    {
        public OtusDbDatabaseFirstContext()
        {
        }

        public OtusDbDatabaseFirstContext(DbContextOptions<OtusDbDatabaseFirstContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Course> Courses { get; set; } = null!;
        public virtual DbSet<SheduleCourse> SheduleCourses { get; set; } = null!;
        public virtual DbSet<SheduleCourseLesson> SheduleCourseLessons { get; set; } = null!;
        public virtual DbSet<Teacher> Teachers { get; set; } = null!;
        public virtual DbSet<СourseСategory> СourseСategories { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql(Global.connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("course");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CourseDefinition)
                    .HasMaxLength(255)
                    .HasColumnName("course_definition");

                entity.Property(e => e.CourseName)
                    .HasMaxLength(100)
                    .HasColumnName("course_name");

                entity.Property(e => e.IdCategory).HasColumnName("id_category");

                entity.HasOne(d => d.IdCategoryNavigation)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.IdCategory)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("course_fk");
            });

            modelBuilder.Entity<SheduleCourse>(entity =>
            {
                entity.ToTable("shedule_course");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.DateEnd).HasColumnName("date_end");

                entity.Property(e => e.DateStart).HasColumnName("date_start");

                entity.Property(e => e.IdCourse).HasColumnName("id_course");

                entity.HasOne(d => d.IdCourseNavigation)
                    .WithMany(p => p.SheduleCourses)
                    .HasForeignKey(d => d.IdCourse)
                    .HasConstraintName("shedule_course_fk");
            });

            modelBuilder.Entity<SheduleCourseLesson>(entity =>
            {
                entity.ToTable("shedule_course_lesson");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.IdSheduleCourse).HasColumnName("id_shedule_course");

                entity.Property(e => e.IdTeacher).HasColumnName("id_teacher");

                entity.Property(e => e.LessonDate).HasColumnName("lesson_date");

                entity.Property(e => e.LessonName)
                    .HasMaxLength(100)
                    .HasColumnName("lesson_name");

                entity.HasOne(d => d.IdSheduleCourseNavigation)
                    .WithMany(p => p.SheduleCourseLessons)
                    .HasForeignKey(d => d.IdSheduleCourse)
                    .HasConstraintName("shedule_course_lesson_fk");

                entity.HasOne(d => d.IdTeacherNavigation)
                    .WithMany(p => p.SheduleCourseLessons)
                    .HasForeignKey(d => d.IdTeacher)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("teacher_fk");
            });

            modelBuilder.Entity<Teacher>(entity =>
            {
                entity.ToTable("teacher");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Email)
                    .HasMaxLength(45)
                    .HasColumnName("email");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(100)
                    .HasColumnName("first_name");

                entity.Property(e => e.LastName)
                    .HasMaxLength(100)
                    .HasColumnName("last_name");
            });

            modelBuilder.Entity<СourseСategory>(entity =>
            {
                entity.ToTable("сourse_сategory");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CategoryDefinition)
                    .HasMaxLength(255)
                    .HasColumnName("category_definition");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(100)
                    .HasColumnName("category_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
