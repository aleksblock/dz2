﻿using System;
using System.Collections.Generic;

namespace DZ_2_DatabaseFirst.Models
{
    public partial class Teacher
    {
        public override string ToString()
        {
            return $"{FirstName} {LastName}"; 
        }

        public Teacher()
        {
            SheduleCourseLessons = new HashSet<SheduleCourseLesson>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; } = null!;
        public string LastName { get; set; } = null!;
        public string Email { get; set; } = null!;

        public virtual ICollection<SheduleCourseLesson> SheduleCourseLessons { get; set; }
    }
}
