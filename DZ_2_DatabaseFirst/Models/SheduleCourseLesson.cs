﻿using System;
using System.Collections.Generic;

namespace DZ_2_DatabaseFirst.Models
{
    public partial class SheduleCourseLesson
    {
        public int Id { get; set; }
        public int IdSheduleCourse { get; set; }
        public string LessonName { get; set; } = null!;
        public DateOnly LessonDate { get; set; }
        public int IdTeacher { get; set; }

        public virtual SheduleCourse IdSheduleCourseNavigation { get; set; } = null!;
        public virtual Teacher IdTeacherNavigation { get; set; } = null!;
    }
}
