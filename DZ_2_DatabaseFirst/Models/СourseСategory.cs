﻿using System;
using System.Collections.Generic;

namespace DZ_2_DatabaseFirst.Models
{
    public partial class СourseСategory
    {
        public СourseСategory()
        {
            Courses = new HashSet<Course>();
        }

        public int Id { get; set; }
        public string CategoryName { get; set; } = null!;
        public string CategoryDefinition { get; set; } = null!;

        public virtual ICollection<Course> Courses { get; set; }
    }
}
