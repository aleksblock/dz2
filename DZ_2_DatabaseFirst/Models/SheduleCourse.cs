﻿using System;
using System.Collections.Generic;

namespace DZ_2_DatabaseFirst.Models
{
    public partial class SheduleCourse
    {
        public SheduleCourse()
        {
            SheduleCourseLessons = new HashSet<SheduleCourseLesson>();
        }

        public int Id { get; set; }
        public int IdCourse { get; set; }
        public DateOnly DateStart { get; set; }
        public DateOnly DateEnd { get; set; }

        public virtual Course IdCourseNavigation { get; set; } = null!;
        public virtual ICollection<SheduleCourseLesson> SheduleCourseLessons { get; set; }
    }
}
