﻿using System;
using System.Collections.Generic;

namespace DZ_2_DatabaseFirst.Models
{
    public partial class Course
    {
        public Course()
        {
            SheduleCourses = new HashSet<SheduleCourse>();
        }

        public int Id { get; set; }
        public int IdCategory { get; set; }
        public string CourseName { get; set; } = null!;
        public string CourseDefinition { get; set; } = null!;

        public virtual СourseСategory IdCategoryNavigation { get; set; } = null!;
        public virtual ICollection<SheduleCourse> SheduleCourses { get; set; }
    }
}
