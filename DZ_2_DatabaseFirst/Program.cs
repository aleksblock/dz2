﻿using Npgsql;
using System;
using DZ_2_DatabaseFirst.Models;
using Microsoft.EntityFrameworkCore;
using DZ_2_DatabaseFirst.Context;

namespace DZ_2_DatabaseFirst
{
    class Program
    {
        //Scaffold-DbContext "Host=localhost;Port=5432;Database=OtusDBCodeFirst;Username=postgres;Password=password;" Npgsql.EntityFrameworkCore.PostgreSQL -OutputDir Models -ContextDir Context -Context OtusContext
        //...
        //Change 2

        static void Main(string[] args)
        {
            ConsoleKeyInfo cki;

            do
            {
                DisplayMenu();
                cki = Console.ReadKey(false);
                switch (cki.Key)
                {
                    case ConsoleKey.D1:
                        GenerateTable();
                        break;
                    case ConsoleKey.D2:
                        InsertValues();
                        break;
                    case ConsoleKey.D3:
                        GetAllRecordFromTablesDB();
                        break;
                    case ConsoleKey.D4:
                        AddValuesIntoTableCourse();
                        break;
                }
            } while (cki.Key != ConsoleKey.D5);

        }

        static void DisplayMenu()
        {
            Console.WriteLine();
            Console.WriteLine("DZ2. PostgreSQL");
            Console.WriteLine();
            Console.WriteLine("1. GenerateTable");
            Console.WriteLine("2. Insert values into tables");
            Console.WriteLine("3. View values from table");
            Console.WriteLine("4. Add value into Course");
            Console.WriteLine("5. Exit");
        }

        /// <summary>
        /// генерация таблиц БД
        /// </summary>
        static void GenerateTable()
        {
            using var connection = new NpgsqlConnection(Global.connectionString);
            connection.Open();

            #region
            var sql = @"
                            DROP TABLE public.shedule_course_lesson ;
                            DROP TABLE public.shedule_course ;
                            DROP TABLE public.teacher ;
                            DROP TABLE public.course ;
                            DROP TABLE public.сourse_сategory ;


                            --КАТЕГОРИИ КУРСОВ
                            CREATE TABLE public.сourse_сategory (
	                            id int4 NOT NULL,
	                            category_name varchar(100) NOT NULL,
	                            category_definition varchar(255) NOT NULL,
	                            CONSTRAINT coursecategory_pk PRIMARY KEY (id)
                            );

                            --КУРСЫ
                            CREATE TABLE public.course (
	                            id int4 NOT NULL,
	                            id_category int4 NOT NULL,
	                            course_name varchar(100) NOT NULL,
	                            course_definition varchar(255) NOT NULL,
	                            CONSTRAINT course_pk PRIMARY KEY (id)
                            );

                            -- public.course foreign keys
                            ALTER TABLE public.course ADD CONSTRAINT course_fk FOREIGN KEY (id_category) REFERENCES public.сourse_сategory(id) ON DELETE RESTRICT;

                            --ПРЕПОДАВАТЕЛИ
                            CREATE TABLE public.teacher (
	                            id int4 NOT NULL,
	                            first_name varchar(100) NOT NULL,
	                            last_name varchar(100) NOT NULL,
	                            email varchar(45) NOT NULL,
	                            CONSTRAINT teacher_pk PRIMARY KEY (id)
                            );

                            --РАСПИСАНИЕ КУРСОВ
                            CREATE TABLE public.shedule_course (
	                            id int4 NOT NULL,
	                            id_course int4 NOT NULL,
	                            date_start date NOT NULL,
	                            date_end date NOT NULL,
	                            CONSTRAINT shedule_course_pk PRIMARY KEY (id)
                            );

                            -- public.shedule_course foreign keys
                            ALTER TABLE public.shedule_course ADD CONSTRAINT shedule_course_fk FOREIGN KEY (id_course) REFERENCES public.course(id) ON DELETE CASCADE;

                            --РАСПИСАНИЕ УРОКОВ НА КУРСАХ
                            CREATE TABLE public.shedule_course_lesson (
	                            id int4 NOT NULL,
	                            id_shedule_course int4 NOT NULL,
	                            lesson_name varchar(100) NOT NULL,
	                            lesson_date date NOT NULL,
                                id_teacher int4 NOT NULL,
	                            CONSTRAINT shedule_course_lesson_pk PRIMARY KEY(id)
                            );

                            -- public.shedule_course_lesson foreign keys
                            ALTER TABLE public.shedule_course_lesson ADD CONSTRAINT shedule_course_lesson_fk FOREIGN KEY(id_shedule_course) REFERENCES public.shedule_course(id) ON DELETE CASCADE;
                            ALTER TABLE public.shedule_course_lesson ADD CONSTRAINT teacher_fk FOREIGN KEY(id_teacher) REFERENCES public.teacher(id) ON DELETE SET NULL;
                            ";
            #endregion

            using var cmd = new NpgsqlCommand(sql, connection);

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine();
            Console.WriteLine($"Created database table. Affected rows count: {affectedRowsCount}");
        }

        /// <summary>
        /// добавление значений в таблицы БД
        /// </summary>
        public static void InsertValues()
        {
            using var connection = new NpgsqlConnection(Global.connectionString);
            connection.Open();

            #region
            //заполнение таблиц БД данными
            var sql = @"
                           --УДАЛЕНИЕ ДАННЫХ
                        delete from course ; 
                        delete from сourse_сategory ;
                        delete from teacher ; 
                        delete from shedule_course ;  
                        delete from shedule_course_lesson ; 

                        --КАТЕГОРИИ КУРСОВ
                        INSERT INTO public.сourse_сategory VALUES(1, 'Программирование', 'Курсы по программированию');
                        INSERT INTO public.сourse_сategory VALUES(2, 'Инфраструктура', 'Курсы по инфраструктуре');
                        INSERT INTO public.сourse_сategory VALUES(3, 'DataScience', 'Курсы по DataScience');
                        INSERT INTO public.сourse_сategory VALUES(4, 'Управление', 'Курсы по управлению');
                        INSERT INTO public.сourse_сategory VALUES(5, 'Аналитика', 'Курсы по аналитике');

                        --КУРСЫ
                        INSERT INTO public.course VALUES(1, 1, 'C# для начинающих программистов', 'Для начинающих');
                        INSERT INTO public.course VALUES(2, 1, 'C# Developer. Basic', 'Для новичков');
                        INSERT INTO public.course VALUES(3, 1, 'C# Developer. Professional', 'Для проффессионалов');
                        INSERT INTO public.course VALUES(4, 2, 'MS SQL Developer', 'Для разработчиков БД');
                        INSERT INTO public.course VALUES(5, 5, 'Аналитик данных', 'Для аналитиков');

                        --ПРЕПОДАВАТЕЛИ
                        INSERT INTO public.teacher VALUES(1, 'Алексей', ' Ягур', 'yag@mail.ru');
                        INSERT INTO public.teacher VALUES(2, 'Роман', ' Приходько', 'rom@mail.ru');
                        INSERT INTO public.teacher VALUES(3, 'Вадим', ' Литвинов', 'vad@mail.ru');

                        --РАСПИСАНИЕ КУРСОВ
                        INSERT INTO public.shedule_course VALUES(1, 3, '28.07.2022', '11.03.2023');

                        --РАСПИСАНИЕ УРОКОВ НА КУРСАХ
                        INSERT INTO public.shedule_course_lesson VALUES(1, 1, 'Знакомство, рассказ о формате Scrum, краткий обзор курса // ДЗ ', '28.07.2022', 1);
                        INSERT INTO public.shedule_course_lesson VALUES(2, 1, 'Архитектура проекта', '04.08.2022', 2);
                        INSERT INTO public.shedule_course_lesson VALUES(3, 1, 'Базы данных: организация работы с потоками данных', '09.08.2022', 3);
                        INSERT INTO public.shedule_course_lesson VALUES(4, 1, 'Базы данных: реляционные базы и работа с ними // ДЗ ', '11.08.2022', 3);
                        INSERT INTO public.shedule_course_lesson VALUES(5, 1, 'Базы данных: NoSQL базы и их особенности', '16.08.2022', 3);";
            #endregion

            using var cmd = new NpgsqlCommand(sql, connection);

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine();
            Console.WriteLine($"Insert values into tables. Affected rows count: {affectedRowsCount}");
        }

        /// <summary>
        /// вывод записей из таблиц БД
        /// </summary>
        public static void GetAllRecordFromTablesDB()
        {
            using (OtusDbDatabaseFirstContext db = new OtusDbDatabaseFirstContext())
            {
                List<Course> courses = db.Courses.Include(c => c.IdCategoryNavigation)
                                                 .Include(sc => sc.SheduleCourses)
                                                 .ThenInclude(scl => scl.SheduleCourseLessons)
                                                 .ThenInclude(t => t.IdTeacherNavigation).ToList();

                foreach(Course c in courses)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Курс {c.Id} : {c.CourseName} | Описание = {c.CourseDefinition} | Категория = {c.IdCategoryNavigation.CategoryName}");

                    foreach (SheduleCourse sc in c.SheduleCourses)
                    {                        
                        Console.WriteLine($"          -> Поток курса № {sc.Id} : Начало = {sc.DateStart.ToShortDateString()} | Окончание = {sc.DateEnd.ToShortDateString()}");
                        
                        foreach (SheduleCourseLesson scl in sc.SheduleCourseLessons)
                        {
                            Console.WriteLine($"            -> Урок № {scl.Id} : {scl.LessonName} | {scl.LessonDate.ToShortDateString()} | {scl.IdTeacherNavigation}");

                        }
                    }
                }

            }
            #region
            //var course_categories = new List<СourseСategory>();
            //var courses = new List<Course>();
            //var teachers = new List<Teacher>();
            //var shedule_courses = new List<SheduleCourse>();
            //var shedule_course_lessons = new List<SheduleCourseLesson>();

            //using (OtusDbDatabaseFirstContext db = new OtusDbDatabaseFirstContext())
            //    course_categories = db.СourseСategories.ToList();

            //Console.WriteLine();
            //Console.WriteLine("Table: course_category");
            //Console.WriteLine("Columns: id | category_name | category_definition");

            //foreach (var c in course_categories)
            //    Console.WriteLine($"{c.Id} | {c.CategoryName} | {c.CategoryDefinition}");

            //...
            //var course = new List<course>();
            //using (OtusDbContext db = new OtusDbContext())
            //    course = db.Courses.Include(p => p.category).ToList();

            //Console.WriteLine(" Table: course");
            //Console.WriteLine(" Columns: id | category | course_name | course_definition");

            //foreach (var c in course)
            //    Console.WriteLine($"{c.id} | {c.category} | {c.course_name} | {c.course_definition}");
            #endregion



        }

        /// <summary>
        /// запись в таблицу Категории Курсов
        /// </summary>
        public static void AddValuesIntoTableCourse()
        {
            using (OtusDbDatabaseFirstContext db = new OtusDbDatabaseFirstContext())
            {
                СourseСategory cc = db.СourseСategories.Where(p => p.Id == 6).FirstOrDefault();
                if (cc!= null)
                {
                    db.СourseСategories.Add(new СourseСategory { Id = 6, CategoryName = "GameDev", CategoryDefinition = "GameDev" });
                    db.SaveChanges();
                    Console.WriteLine($"Added 6, GameDev, GameDev ");
                }
                else
                    Console.WriteLine($"Id = 6 exist in database!");
            }
        }
    }
}
