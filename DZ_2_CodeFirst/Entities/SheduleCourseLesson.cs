﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_CodeFirst.Entities
{
    public class SheduleCourseLesson
    {
        public int Id { get; set; }
        public int IdSheduleCourse { get; set; }
        public SheduleCourse SheduleCourse { get; set; }
        public string LessonName { get; set; } = String.Empty;
        public DateTime LessonDate { get; set; }
        public int? IdTeacher { get; set; }
        public Teacher Teacher { get; set; }
    }


}
