﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_CodeFirst.Entities
{
    public class SheduleCourse
    {
        public SheduleCourse()
        {
            CourseLessons = new List<SheduleCourseLesson>();
        }

        public int Id { get; set; }
        public int IdCourse { get; set; }
        public Course Course { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        public List<SheduleCourseLesson> CourseLessons { get; set; }
    }


}
