﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_CodeFirst.Entities
{
    public class Teacher
    {
        public Teacher()
        {
            TeacherLessons = new List<SheduleCourseLesson>();
        }

        public int Id { get; set; }
        public int FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<SheduleCourseLesson> TeacherLessons { get; set; }


    }


}
