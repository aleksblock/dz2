﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_CodeFirst.Entities
{
    public class Course
    {
        public int Id { get; set; }
        public int Id_category { get; set; }
        public CourseCategory Category { get; set; }
        public string CourseName { get; set; }
        public string CourseDefinition { get; set; }

    }


}
