﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DZ_2_CodeFirst.Entities
{
    public class CourseCategory
    {

        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryDefinition { get; set; }
        public List<Course> Courses { get; set; }


    }


}
