﻿using Npgsql;
using System;
using Microsoft.EntityFrameworkCore;
using DZ_2_CodeFirst.Entities;

namespace DZ_2_CodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            var course_categories = new List<CourseCategory>();
            using (OtusDbCodeFirstContext db = new OtusDbCodeFirstContext())
                course_categories = db.CourseCategories.ToList();

            Console.WriteLine(" Table: CourseCategories");
            Console.WriteLine(" Columns: id | category_name | category_definition");

            foreach (var c in course_categories)
                Console.WriteLine($"{c.Id} | {c.CategoryName} | {c.CategoryDefinition}");
        }
    }
}
